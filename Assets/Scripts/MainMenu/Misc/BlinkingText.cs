﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BlinkingText : MonoBehaviour {

	public float blinkingFrequency = 1;
	private Text textTarget;

	//Misc Operator.
	protected bool isLooping = true;

	//Color operator.
	Color zeroAplha = new Color(0, 0, 0, 0);
	Color opaqueAlpha = new Color(1, 1, 1, 1);

	void Start () {

		//Getting an Component.
		textTarget = (Text) GetComponent(typeof(Text));

		//Start blinking coroutine.
		StartCoroutine(BlinkingNumerator());
	}
	
	public void StopFading() {
		isLooping = false;
		textTarget.CrossFadeAlpha(0, blinkingFrequency, true);
		textTarget.color = zeroAplha;
	}

	IEnumerator BlinkingNumerator()
	{

		textTarget.CrossFadeAlpha(0, blinkingFrequency, true);
		yield return new WaitForSeconds(blinkingFrequency);
		textTarget.CrossFadeAlpha(1, blinkingFrequency, true);
		yield return new WaitForSeconds(blinkingFrequency);

		//Creating Looping.
		if(isLooping)
			StartCoroutine(BlinkingNumerator());

	}
}
