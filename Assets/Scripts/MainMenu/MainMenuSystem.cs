﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuSystem : MonoBehaviour {

	private enum LogoVisibility {Visible, Invisible}

	//Startup Game Operator.
	public Image[] gameLogoImage;
	public float logoFadeTime;
	public Text anyTouchTriggerText;
	private bool pressAnyTouchSequnces = true;

	private void Start()
	{
		//Enabling to simulate mith mouse.
		Input.simulateMouseWithTouches = true;
	}

	// Update is called once per frame
	void Update () {
		
		//if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && pressAnyTouchSequnces)
		if(Input.anyKeyDown)
		{
			pressAnyTouchSequnces = false;
			anyTouchTriggerText.GetComponent<BlinkingText>().StopFading();
			GameLogoVisibility(LogoVisibility.Invisible);
		}
	}

	private void GameLogoVisibility(LogoVisibility logoVisibility)
	{
		switch (logoVisibility)
		{
			case LogoVisibility.Visible:
				foreach (Image lImage in gameLogoImage)
				{
					lImage.CrossFadeAlpha(255, logoFadeTime, false);
				}
				break;
			case LogoVisibility.Invisible:
				foreach (Image lImage in gameLogoImage)
				{
					lImage.CrossFadeAlpha(0, logoFadeTime, false);
				}
				break;
		}
	}
}
