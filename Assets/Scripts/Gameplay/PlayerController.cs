﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EXY.Gameplay { 

    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour {
    
        [Header("Player General Settings")]
        public float movementSpeed;

        //RigidBody Data References.
        private Rigidbody playerRigidbody;

        //Misc Operator...
        bool facingRight = false;
        bool facingLeft = false;

		//Initializing the Player first.
        void Start () {
            InitializingRigidBodySystem();
	    }

		//Update every frames.
	    void Update () {
            PlayerNavigationController();
	    }

        /// <summary>
        /// Initializing the overall RigidBody System.
        /// </summary>
        private void InitializingRigidBodySystem()
        {
            playerRigidbody = GetComponent<Rigidbody>();
            playerRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }

        private void PlayerNavigationController()
        {

            //Get Horizontal input float data.
            float inputX = Input.GetAxis("Horizontal");

            //Right Direction
            if (inputX > 0){
                print("Movement Right");
            }

            //Left Direction
            else if(inputX < 0)
            {
                print("Movement Left");
            }

        }
    }
}
