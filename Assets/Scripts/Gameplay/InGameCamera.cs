﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera), typeof(Rigidbody))]
public class InGameCamera : MonoBehaviour {

	public enum PlayerDirection {LeftDirection, RightDirection}
	protected PlayerDirection playerDirection;

	public GameObject playerReferences;
	public Vector3 cameraOffsetPivot;

	private Camera playerCamera;
	private Rigidbody cameraRigidbody;

	//Misc operator.
	public bool isFlipPivot = false;
	private float smoothVelocity;

	//Init Camera gameplay system.
	void Awake () {
		playerCamera = GetComponent(typeof(Camera)) as Camera ;
		cameraRigidbody = GetComponent(typeof(Rigidbody)) as Rigidbody;

		InitializingRigidBody();
		InitializingDirection();

		Debug.Log(GetCameraTallOffset);
	}
	
	private void InitializingRigidBody() {
		cameraRigidbody.freezeRotation = true;
		cameraRigidbody.isKinematic = true;
		cameraRigidbody.useGravity = false;
	}
	private void InitializingDirection()
	{
		if (!isFlipPivot)
			playerDirection = PlayerDirection.RightDirection;
		else
			playerDirection = PlayerDirection.RightDirection;
	}

	private void OnDrawGizmos()
	{
		if (isFlipPivot)
			Gizmos.DrawWireSphere(CameraOffsetPivot(Vector3.left), 0.5f);

		else if(!isFlipPivot)
			Gizmos.DrawWireSphere(CameraOffsetPivot(Vector3.right), 0.5f);
		
		Gizmos.color = Color.yellow;
	}

	private Vector3 CameraOffsetPivot(Vector3 direction)
	{
		Vector3 camOffset = Vector3.zero;

		if (direction == Vector3.right)
			camOffset = (playerReferences.transform.position + cameraOffsetPivot);
		else if (direction == Vector3.left)
			camOffset = (playerReferences.transform.position - cameraOffsetPivot);

		camOffset.y = cameraOffsetPivot.y;
		return camOffset;
	}
	public float GetCameraTallOffset
	{
		get
		{
			float tallOffset = 0;

			if(playerDirection == PlayerDirection.RightDirection || playerDirection == PlayerDirection.LeftDirection)
				return CameraOffsetPivot(isFlipPivot ? Vector3.left : Vector3.right).y;

			return tallOffset;
		}
	}

}
